package gui;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class PSOTabController {

    @FXML
    private TextField c1;

    @FXML
    private TextField c2;

    @FXML
    private TextField w;

    private TextArea helpText;
    private MainWindowController mainWindowController;

    public String getC1Value() {
        return c1.getText();
    }

    public String getC2Value() {
        return c2.getText();
    }

    public String getWValue() {
        return w.getText();
    }


    public void setHelpText(TextArea helpText) {
        this.helpText = helpText;
    }

    @FXML
    void c1Help(MouseEvent event) {
        helpText.setText("Zadej učící parametr C1 pro algoritmus.\\nParametr musí být kladné číslo.");
    }

    @FXML
    void c2Help(MouseEvent event) {
        helpText.setText("Zadej učící parametr C2 pro algoritmus.\nParametr musí být kladné číslo.");
    }

    @FXML
    void wHelp(MouseEvent event) {
        helpText.setText("Zadej parametr setrvačnosi pro algoritmus.\nParametr musí být kladné číslo.");
    }

}
