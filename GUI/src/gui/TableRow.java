package gui;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class TableRow {
   private SimpleStringProperty dimension;
   private SimpleStringProperty upperBoundary;
   private SimpleStringProperty lowerBoundary;


    public TableRow(String dimension, String upperBoundary, String lowerBoundary) {
        this.dimension = new SimpleStringProperty(dimension);
        this.upperBoundary = new SimpleStringProperty(upperBoundary);
        this.lowerBoundary = new SimpleStringProperty(lowerBoundary);
    }

    public String getDimension() {
        return dimension.get();
    }

    public SimpleStringProperty dimensionProperty() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension.set(dimension);
    }

    public String getUpperBoundary() {
        return upperBoundary.get();
    }

    public SimpleStringProperty upperBoundaryProperty() {
        return upperBoundary;
    }

    public void setUpperBoundary(String upperBoundary) {
        this.upperBoundary.set(upperBoundary);
    }

    public String getLowerBoundary() {
        return lowerBoundary.get();
    }

    public SimpleStringProperty lowerBoundaryProperty() {
        return lowerBoundary;
    }

    public void setLowerBoundary(String lowerBoundary) {
        this.lowerBoundary.set(lowerBoundary);
    }
}
