package gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;

public class ChartWindowController {

    @FXML
    private LineChart<Integer, Double> convertionChart;

    @FXML
    private NumberAxis fitnessAxis;

    @FXML
    private CategoryAxis iterationAxis;

    @FXML
    private Button okButton;
    private double[] fitness;
    public void setData(double[] fitness) {
        this.fitness = fitness;

        List<XYChart.Data<Integer,Double>> seriesData = new ArrayList<>();
        for(int i=0;i<fitness.length;++i)
            seriesData.add(new XYChart.Data(Integer.toString(i),fitness[i]));

        XYChart.Series<Integer,Double> series = new XYChart.Series<>();
        series.getData().addAll(seriesData);
        convertionChart.getData().add(series);

    }

    public void initialize(){

    }

}
