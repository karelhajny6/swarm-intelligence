package swarm;

public class Bat extends Agent {

    private double loudness;
    private double pulsRate;
    private double[] velocity;
    private double frequency;

    public double getLoudness() {
        return loudness;
    }

    public void setLoudness(double loudness) {
        this.loudness = loudness;
    }

    public double getPulsRate() {
        return pulsRate;
    }

    public void setPulsRate(double pulsRate) {
        this.pulsRate = pulsRate;
    }

    public double[] getVelocity() {
        return velocity.clone();
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity.clone();
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }
}
