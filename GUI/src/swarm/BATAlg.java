package swarm;

import swarm.boundary.Boundary;
import swarm.functions.FitnessFunction;

import java.util.ArrayList;
import java.util.Random;

public class BATAlg implements SwarmAlgorithm {

    private int populationSize;
    private int dimensionSize;
    private ArrayList<Bat> population = new ArrayList<Bat>();
    private float maxFrequency;
    private float minFrequency;
    private Boundary boundary;
    private FitnessFunction function;
    private final double[] upperBoundaries;
    private final double[] lowerBoundaries;
    private final int iterationNumber;
    private final Random random = new Random();
    private final float rStart;
    private Bat gBest = new Bat();
    private float alfa;
    private float gama;
    private float startLoudness = 1;

    public BATAlg(int populationSize, int dimensionSize, double[] upperBoundaries, double[] lowerBoundaries,
                  int iterationNumber, FitnessFunction function, Boundary boundary, float minFrequency, float maxFrequency, float rStart, float alfa, float gama) {


        if (populationSize < 2) {
            throw new IllegalArgumentException("Invalid population value: " + populationSize);
        }
        if (dimensionSize < 2) {
            throw new IllegalArgumentException("Invalid dimension size value: " + dimensionSize);
        }
        if (lowerBoundaries == null || lowerBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid lower boundaries value");
        }
        if (upperBoundaries == null || upperBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid upper boundaries value");
        }
        if (iterationNumber < 1) {
            throw new IllegalArgumentException("Invalid number of iteration: " + iterationNumber);
        }
        if (function == null) {
            throw new IllegalArgumentException("Invalid fitness function");
        }
        if (boundary == null) {
            throw new IllegalArgumentException("Invalid boundary");
        }
        this.function = function;
        this.populationSize = populationSize;
        this.dimensionSize = dimensionSize;
        this.lowerBoundaries = lowerBoundaries.clone();
        this.upperBoundaries = upperBoundaries.clone();
        this.iterationNumber = iterationNumber;
        this.boundary = boundary;
        this.minFrequency = minFrequency;
        this.maxFrequency = maxFrequency;
        this.rStart = rStart;
        this.alfa = alfa;
        this.gama = gama;
    }

    public Result runAlgorithm() {
        this.initializePopulation();
        long startTime = System.currentTimeMillis();
        double[] newVelocity = new double[this.getDimensionSize()];
        double[] newPosition = new double[this.getDimensionSize()];
        double newFitness;
        double[] convergence = new double[this.getIterationNumber()];
        for (int i = 0; i < getIterationNumber(); i++) {
            for (Bat bat : getPopulation()) {
                bat.setFrequency(getMinFrequency() + (getMinFrequency() - getMaxFrequency()) * random.nextFloat());
                for (int j = 0; j < this.getDimensionSize(); j++) {
                    newVelocity[j] = bat.getVelocity()[j] + (bat.getPosition()[j] - gBest.getPosition()[j]) * bat.getFrequency();
                }
                bat.setVelocity(newVelocity);
                for (int j = 0; j < this.getDimensionSize(); j++) {
                    newPosition[j] = bat.getVelocity()[j] + bat.getPosition()[j];
                }
                newPosition = boundary.moveCoordinatesIntoBoundary(newPosition, getUpperBoundaries(), getLowerBoundaries());
                if (random.nextFloat() > bat.getPulsRate()) {
                    for (int j = 0; j < this.getDimensionSize(); j++) {
                        newPosition[j] = this.getgBest().getPosition()[j] + 0.01 * random.nextFloat() * getAveragePulsRate();
                    }
                }
                newFitness = getFunction().getFitness(newPosition);
                if (newFitness < bat.getFitnessValue() && bat.getLoudness() > random.nextDouble()) {
                    bat.setPosition(newPosition);
                    bat.setFitnessValue(this.getFunction().getFitness(bat.getPosition()));
                    bat.setLoudness(bat.getLoudness() * this.getAlfa());
                    bat.setPulsRate(this.getrStart() * (1 - Math.exp(-this.getGama() * (i + 1))));
                }

                if (newFitness < getgBest().getFitnessValue()) {
                    bat.setPosition(newPosition);
                    setgBest(bat);
                    System.out.println(bat.getFitnessValue());
                }


            }
            convergence[i] = this.gBest.getFitnessValue();
        }
        long endTime = System.currentTimeMillis();
        return new Result(getgBest(), getIterationNumber(), startTime, endTime, convergence,getFunction().getName(),AlgorithmType.BAT, getMinFrequency(), getMaxFrequency(), getrStart(), getAlfa(), getGama());
    }

    private double getAveragePulsRate() {
        double sum = 0;
        for (Bat bat : this.getPopulation()) {
            sum = bat.getLoudness() + sum;
        }
        return sum / this.getPopulationSize();
    }

    public void initializePopulation() {
        createPopulation(this.populationSize);
        double[] position = new double[this.getDimensionSize()];
        double[] velocity = new double[this.getDimensionSize()];
        if (gBest == null) {
            setgBest(getPopulation().get(0));
        }
        for (Bat bat : population) {
            for (int i = 0; i < getDimensionSize(); i++) {
                position[i] = this.getLowerBoundaries()[i] + (this.getUpperBoundaries()[i] - this.getLowerBoundaries()[i]) *
                        this.getRandom().nextDouble();
                velocity[i] = 0;
            }
            bat.setVelocity(velocity);
            bat.setPosition(position);
            bat.setFitnessValue(function.getFitness(bat.getPosition()));
            bat.setPulsRate(this.getrStart());
            bat.setLoudness(startLoudness);

        }
        setgBest(getPopulation().get(0));
    }

    private void createPopulation(int popSize) {
        for (int i = 0; i < popSize; i++) {
            this.population.add(new Bat());
        }
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public int getDimensionSize() {
        return dimensionSize;
    }

    public void setDimensionSize(int dimensionSize) {
        this.dimensionSize = dimensionSize;
    }

    public ArrayList<Bat> getPopulation() {
        return population;
    }

    public void setPopulation(ArrayList<Bat> population) {
        this.population = population;
    }

    public float getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(float maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public float getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(float minFrequency) {
        this.minFrequency = minFrequency;
    }

    public Boundary getBoundary() {
        return boundary;
    }

    public void setBoundary(Boundary boundary) {
        this.boundary = boundary;
    }

    public FitnessFunction getFunction() {
        return function;
    }

    public void setFunction(FitnessFunction function) {
        this.function = function;
    }

    public double[] getUpperBoundaries() {
        return upperBoundaries;
    }

    public double[] getLowerBoundaries() {
        return lowerBoundaries;
    }

    public int getIterationNumber() {
        return iterationNumber;
    }

    public Random getRandom() {
        return random;
    }


    public Bat getgBest() {
        return gBest;
    }

    public void setgBest(Bat gBest) {
        this.gBest.setPosition(gBest.getPosition().clone());
        this.gBest.setLoudness(gBest.getLoudness());
        this.gBest.setPulsRate(gBest.getPulsRate());
        this.gBest.setVelocity(gBest.getVelocity().clone());
        this.gBest.setFitnessValue(gBest.getFitnessValue());
        this.gBest.setFrequency(gBest.getFrequency());
    }

    public float getAlfa() {
        return alfa;
    }

    public void setAlfa(float alfa) {
        this.alfa = alfa;
    }

    public float getGama() {
        return gama;
    }

    public void setGama(float gama) {
        this.gama = gama;
    }

    public float getrStart() {
        return rStart;
    }

}
