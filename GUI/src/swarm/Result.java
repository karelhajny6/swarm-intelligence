package swarm;

import swarm.functions.FunctionType;

import java.util.ArrayList;

public class Result {
    private Agent agent;
    private int iterations;
    private long startTime;
    private long endTime;
    private double[] convergence;
    private double[] parameters;
    private String functionName;
    private AlgorithmType algorithmType;

    public Result(Agent agent, int iterations, long startTime, long endTime, double[] convergence, String functionName, AlgorithmType algorithmType, double... parameters) {
        this.agent = agent;
        this.iterations = iterations;
        this.startTime = startTime;
        this.endTime = endTime;
        this.convergence = convergence;
        this.parameters = parameters;
        this.functionName = functionName;
        this.algorithmType = algorithmType;
    }


    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double[] getConvergence() {
        return convergence.clone();
    }

    public void setConvergence(double[] convergence) {
        this.convergence = convergence;
    }

    public long getCalculationTime() {
        return endTime - startTime;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "Result{" +
                "agent=" + agent +
                ", iterations=" + iterations +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }

    public String getStructuredOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append("Výsledek optimalizace: \n");
        sb.append("Nejlepší nalezená fitness hodnota: " + getAgent().getFitnessValue() + "\n");
        sb.append("Souřadnice nalezeného minima:" + agent.getCoordinates() + "\n");
        sb.append("Čas výpočtu: " + getCalculationTime() + "ms \n");

        return sb.toString();
    }

    public String getStructuredExcelOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.algorithmType.name() + "\t");
        sb.append(this.functionName + "\t");
        sb.append(iterations + "\t");
        sb.append(agent.getFitnessValue() + "\t");
        for (int i = 0; i < this.parameters.length; i++) {
            sb.append(parameters[i] + "\t");
        }
        sb.append("\n");
        return sb.toString();
    }
}
