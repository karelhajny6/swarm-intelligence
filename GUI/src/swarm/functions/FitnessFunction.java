package swarm.functions;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class FitnessFunction {

    private final double dimensionSize;

    public FitnessFunction(double dimensionSize) {
        if (dimensionSize < 2) {
            throw new IllegalArgumentException("Invalid dimension size: " + dimensionSize);
        }
        this.dimensionSize = dimensionSize;
    }

    public double getDimensionSize() {
        return dimensionSize;
    }

    public abstract double getFitness(double[] position);
    public static Image getFunctionImage(String equation){
        TeXFormula formula = new TeXFormula(equation);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics2D = image.createGraphics();
        graphics2D.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
        Image awtImage = SwingFXUtils.toFXImage((BufferedImage) (formula.createBufferedImage(TeXConstants.STYLE_TEXT, 64, java.awt.Color.BLACK, Color.GRAY)), null);
        return awtImage;
    }
    public abstract String getName();

}
