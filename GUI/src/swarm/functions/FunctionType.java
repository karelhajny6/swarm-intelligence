package swarm.functions;

public enum FunctionType {

    RASTRIG("Rastrigova funkce"),
    GRIEWANKS("Grievangava funkce"),
    WEIERSTRASS("Weierstrassova funkce"),
    ACKLEY("Ackley funkce"),
    HAPPYCAT("Happy Cat funkce");

    private final String name;

    FunctionType(String s) {
        this.name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
