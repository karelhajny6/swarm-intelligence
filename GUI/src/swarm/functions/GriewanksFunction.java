package swarm.functions;

public class GriewanksFunction extends FitnessFunction {
    public GriewanksFunction(double dimensionSize) {
        super(dimensionSize);
    }

    @Override
    public double getFitness(double[] position) {
        int d = position.length;
        double suma = 0;
        double mult = 1;
        for (int i = 1; i <= d; i++) {
            suma = suma + (Math.pow(position[i - 1], 2) / 4000);
        }
        for (int i = 1; i <= d; i++) {
            mult = mult * (Math.cos(position[i - 1] / Math.sqrt(i)) + 1);
        }
        double result = suma - mult;
        return result;
    }

    public String getName() {
        return FunctionType.RASTRIG.name();
    }


}
