package swarm.functions;

public class RastrigFunction extends FitnessFunction {

    public RastrigFunction(double dimensionSize) {
        super(dimensionSize);
    }

    public double getFitness(double[] position) {
        double sum = 0;
        for (int i = 0; i < position.length; i++) {
            sum += Math.pow(position[i], 2) - 10 * Math.cos(2 * Math.PI * position[i]);
        }
        return sum + 10 * this.getDimensionSize();
    }

    @Override
    public String getName() {
        return FunctionType.RASTRIG.name();
    }


}
