package swarm.functions;

public class AckleyFunction extends FitnessFunction {

    public AckleyFunction(double dimensionSize) {
        super(dimensionSize);
    }

    @Override
    public double getFitness(double[] position) {
        double sum1 = 0;
        double sum2 = 0;
        for (int i = 0; i < position.length; i++) {
            sum1 += Math.pow(position[i], 2);
            sum2 += (Math.cos(2 * Math.PI * position[i]));
        }

        return -20.0 * Math.exp(-0.2 * Math.sqrt(sum1 / ((double) position.length)))
                - Math.exp(sum2 / ((double) position.length)) + 20 + Math.exp(1.0);
    }

    @Override
    public String getName() {
        return FunctionType.ACKLEY.name();
    }
}
