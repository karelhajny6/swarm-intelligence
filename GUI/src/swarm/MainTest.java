package swarm;

import swarm.boundary.SimpleBoundary;
import swarm.functions.RastrigFunction;

public class MainTest {
    public static void main(String[] args) {
      //  PSO pso = new PSO(20,2,new double[]{5.12,5.12}, new double[]{-5.12,-5.12}, 100,new RastrigFunction(2),new SimpleBoundary(),1,1, 1);
       // Result result = pso.runAlgorithm();
        //System.out.println(result.getAgent().toString());

        WSA gwo = new WSA(100,2,new double[]{5.12,5.12}, new double[]{-5.12,-5.12},
                5000,new RastrigFunction(2),new SimpleBoundary(),2,1);
            Result result = gwo.runAlgorithm();
        System.out.println(result);
    }
}
