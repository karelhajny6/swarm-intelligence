package swarm;

import swarm.boundary.Boundary;
import swarm.functions.FitnessFunction;

import java.util.ArrayList;
import java.util.Random;

public class WSA implements SwarmAlgorithm {

    private final Boundary boundary;
    private final int populationSize;
    private final int dimensionSize;
    private ArrayList<Whale> population = new ArrayList<Whale>();
    private Whale gBest = new Whale();
    private final double[] upperBoundaries;
    private final double[] lowerBoundaries;
    private final int iterationNumber;
    private final FitnessFunction function;
    private final Random random = new Random();
    private float ro;
    private float eta;
    private Result result;

    public WSA(int populationSize, int dimensionSize, double[] upperBoundaries, double[] lowerBoundaries, int iterationNumber, FitnessFunction function, Boundary boundary, float ro, float eta) {
        this.boundary = boundary;
        this.populationSize = populationSize;
        this.dimensionSize = dimensionSize;
        this.upperBoundaries = upperBoundaries;
        this.lowerBoundaries = lowerBoundaries;
        this.iterationNumber = iterationNumber;
        this.function = function;
        this.ro = ro;
        this.eta = eta;
    }

    public Result runAlgorithm() {
        long startTime = System.currentTimeMillis();
        initializePopulation();
        Whale nearest;
        double[] convergence = new double[this.iterationNumber];
        double[] newpos = new double[this.getDimensionSize()];
        for (int i = 0; i < getIterationNumber(); i++) {
            for (Whale whale : getPopulation()) {
                nearest = getBetterAndNearest(whale);
                if (nearest != null) {
                    double param = ro * Math.exp(-eta * getEuklideanDistance(nearest, whale));
                    double randomValue = 0 + (param - 0) * random.nextDouble();
                    for (int j = 0; j < getDimensionSize(); j++) {
                        newpos[j] = whale.getPosition()[j] + randomValue * (nearest.getPosition()[j] - whale.getPosition()[j]);
                    }
                    newpos = boundary.moveCoordinatesIntoBoundary(newpos, upperBoundaries, lowerBoundaries);
                    whale.setPosition(newpos);
                    whale.setFitnessValue(getFunction().getFitness(whale.getPosition()));
                    nearest = null;
                }
            }
            System.out.println(getBestWhale().toString());
            convergence[i] = gBest.getFitnessValue();
        }
        long endTime = System.currentTimeMillis();
        return new Result(getBestWhale(), getIterationNumber(), startTime, endTime, convergence, getFunction().getName(), AlgorithmType.WSA, getRo(), getEta());
    }

    private Whale getBestWhale() {
        Whale best = getPopulation().get(0);
        for (Whale whale : getPopulation()) {
            if (whale.getFitnessValue() < best.getFitnessValue()) {
                best = whale;
            }
        }
        return best;
    }

    public void initializePopulation() {

        createPopulation(this.populationSize);
        double[] position = new double[this.getDimensionSize()];
        for (Whale whale : population) {
            for (int i = 0; i < getDimensionSize(); i++) {
                position[i] = this.getLowerBoundaries()[i] + (this.getUpperBoundaries()[i] - this.getLowerBoundaries()[i]) *
                        this.getRandom().nextDouble();
            }
            whale.setPosition(position);
            whale.setFitnessValue(function.getFitness(whale.getPosition()));
        }

    }

    private float getEuklideanDistance(Whale w1, Whale w2) {
        float distance = 0;
        for (int i = 0; i < this.getDimensionSize(); i++) {
            distance += Math.pow(w1.getPosition()[i] - w2.getPosition()[i], 2);
        }
        distance = (float) Math.sqrt(distance);
        return distance;
    }

    private void createPopulation(int populationSize) {
        for (int i = 0; i < this.getPopulationSize(); i++) {
            this.getPopulation().add(new Whale());
        }
    }

    private Whale getBetterAndNearest(Whale whale) {
        int v = 0;
        float temp = Float.MAX_VALUE;
        float distance;
        Whale bestMatch = null;
        for (Whale w : this.getPopulation()) {
            if (whale != w) {
                if (w.getFitnessValue() < whale.getFitnessValue()) {
                    distance = getEuklideanDistance(whale, w);
                    if (distance < temp) {
                        bestMatch = w;
                        temp = distance;
                    }
                }
            }
        }
        return bestMatch;
    }

    public Boundary getBoundary() {
        return boundary;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getDimensionSize() {
        return dimensionSize;
    }

    public ArrayList<Whale> getPopulation() {
        return population;
    }

    public void setPopulation(ArrayList<Whale> population) {
        this.population = population;
    }

    public Whale getgBest() {
        return gBest;
    }

    public void setgBest(Whale gBest) {
        this.gBest = gBest;
    }

    public double[] getUpperBoundaries() {
        return upperBoundaries;
    }

    public double[] getLowerBoundaries() {
        return lowerBoundaries;
    }

    public int getIterationNumber() {
        return iterationNumber;
    }

    public FitnessFunction getFunction() {
        return function;
    }

    public Random getRandom() {
        return random;
    }

    public float getRo() {
        return ro;
    }

    public void setRo(float ro) {
        this.ro = ro;
    }

    public float getEta() {
        return eta;
    }

    public void setEta(float eta) {
        this.eta = eta;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
