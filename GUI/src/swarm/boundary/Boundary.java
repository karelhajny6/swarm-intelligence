package swarm.boundary;

public interface Boundary {

     double[] moveCoordinatesIntoBoundary(double[] coordinates, double[] upperBoundary, double[] lowerBoundary);
}
