package swarm.boundary;

import java.util.Random;

public class RandomBoundary implements Boundary {

    @Override
    public double[] moveCoordinatesIntoBoundary(double[] coordinates, double[] upperBoundary, double[] lowerBoundary) {
        Random random = new Random();
        for (int i = 0; coordinates.length < i; i++) {
            coordinates[i] = lowerBoundary[i] + (upperBoundary[i] - lowerBoundary[i]) *
                    random.nextDouble();
        }
        return coordinates;
    }
}
