package swarm;

import swarm.boundary.Boundary;
import swarm.functions.FitnessFunction;

import java.util.ArrayList;
import java.util.Random;

public class GWO implements SwarmAlgorithm {
    private final Boundary boundary;
    private final int populationSize;
    private final int dimensionSize;
    private ArrayList<Wolf> population = new ArrayList<Wolf>();
    private final double[] upperBoundaries;
    private final double[] lowerBoundaries;
    private final int iterationNumber;
    private final FitnessFunction function;
    private final Random random = new Random();
    private float a;
    private Wolf alpha = new Wolf();
    private Wolf beta = new Wolf();
    private Wolf delta = new Wolf();
    private Result result;

    public GWO(int populationSize, int dimensionSize, double[] upperBoundaries, double[] lowerBoundaries,
               int iterationNumber, FitnessFunction function, Boundary boundary, float a) {


        if (populationSize < 4) {
            throw new IllegalArgumentException("Invalid population value: " + populationSize);
        }
        if (dimensionSize < 2) {
            throw new IllegalArgumentException("Invalid dimension size value: " + dimensionSize);
        }
        if (lowerBoundaries == null || lowerBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid lower boundaries value");
        }
        if (upperBoundaries == null || upperBoundaries.length < dimensionSize) {
            throw new IllegalArgumentException("Invalid upper boundaries value");
        }
        if (iterationNumber < 1) {
            throw new IllegalArgumentException("Invalid number of iteration: " + iterationNumber);
        }
        if (function == null) {
            throw new IllegalArgumentException("Invalid fitness function");
        }
        if (boundary == null) {
            throw new IllegalArgumentException("Invalid boundary");
        }
        this.function = function;
        this.populationSize = populationSize;
        this.dimensionSize = dimensionSize;
        this.lowerBoundaries = lowerBoundaries.clone();
        this.upperBoundaries = upperBoundaries.clone();
        this.iterationNumber = iterationNumber;
        this.a = a;
        this.boundary = boundary;
    }

    //deciding the hierarchy of wolfs
    private void createHierarchy() {
        for (Wolf wolf : this.getPopulation()) {
            if (wolf.getFitnessValue() < alpha.getFitnessValue()) {
                alpha = wolf;
            } else if (wolf.getFitnessValue() < beta.getFitnessValue()) {
                beta = wolf;
            } else if (wolf.getFitnessValue() < delta.getFitnessValue()) {
                delta = wolf;
            }
        }
    }

    public Result runAlgorithm() {
        long startTime = System.currentTimeMillis();
        double[] newPossition = new double[this.getDimensionSize()];
        initializePopulation();
        float a_step = 2 / getIterationNumber();
        double[] convergence = new double[this.iterationNumber];
        for (int i = 0; i < getIterationNumber(); i++) {
            createHierarchy();
            //actualization of parameter for switch between search and ...
            a -= a_step;
            //movement of wolfs
            for (Wolf wolf : this.getPopulation()) {
                for (int j = 0; j < this.getDimensionSize(); j++) {
                    float r1 = random.nextFloat();
                    float r2 = random.nextFloat();
                    float A1 = 2 * a * r1 - a;  // Equation (3.3)
                    float C1 = 2 * r2;  // Equation (3.4)

                    double D_alpha = Math.abs(C1 * this.alpha.getPosition()[j] - wolf.getPosition()[j]);  //# Equation (3.5)-part 1
                    double X1 = alpha.getPosition()[j] - A1 * D_alpha; // # Equation (3.6)-part 1

                    r1 = random.nextFloat();
                    r2 = random.nextFloat();

                    float A2 = 2 * a * r1 - a;  //# Equation (3.3)
                    float C2 = 2 * r2;  //# Equation (3.4)

                    double D_beta = Math.abs(C2 * beta.getPosition()[j] - wolf.getPosition()[j]);  //# Equation (3.5)-part 2
                    double X2 = beta.getPosition()[j] - A2 * D_beta;  //# Equation (3.6)-part 2

                    r1 = random.nextFloat();
                    r2 = random.nextFloat();

                    float A3 = 2 * a * r1 - a; //# Equation (3.3)
                    float C3 = 2 * r2;  //# Equation (3.4)

                    double D_delta = Math.abs(C3 * delta.getPosition()[j] - wolf.getPosition()[j]);  //# Equation (3.5)-part 3
                    double X3 = delta.getPosition()[j] - A3 * D_delta;  //# Equation (3.5)-part 3

                    newPossition[j] = (X1 + X2 + X3) / 3;
                    wolf.setPosition(newPossition);  //# Equation (3.7)
                }
                wolf.setFitnessValue(function.getFitness(wolf.getPosition()));
            }
            System.out.println(alpha.toString());
            convergence[i] = this.alpha.getFitnessValue();
        }
        long endTime = System.currentTimeMillis();
        return new Result(this.alpha, this.getIterationNumber(), startTime, endTime, convergence,getFunction().getName(),AlgorithmType.GWO, getA());
    }


    public void initializePopulation() {
        createPopulation(this.populationSize);
        double[] position = new double[this.getDimensionSize()];
        for (Wolf wolf : population) {
            for (int i = 0; i < getDimensionSize(); i++) {
                position[i] = this.getLowerBoundaries()[i] + (this.getUpperBoundaries()[i] - this.getLowerBoundaries()[i]) *
                        this.getRandom().nextDouble();
            }
            wolf.setPosition(position);
            wolf.setFitnessValue(function.getFitness(wolf.getPosition()));
        }
    }

    private void createPopulation(int popSize) {
        for (int i = 0; i < popSize; i++) {
            this.population.add(new Wolf());
        }
    }

    public Boundary getBoundary() {
        return boundary;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getDimensionSize() {
        return dimensionSize;
    }

    public ArrayList<Wolf> getPopulation() {
        return population;
    }

    public void setPopulation(ArrayList<Wolf> population) {
        this.population = population;
    }

    public double[] getUpperBoundaries() {
        return upperBoundaries;
    }

    public double[] getLowerBoundaries() {
        return lowerBoundaries;
    }

    public int getIterationNumber() {
        return iterationNumber;
    }

    public FitnessFunction getFunction() {
        return function;
    }

    public Random getRandom() {
        return random;
    }

    public float getA() {
        return a;
    }

    public Wolf getAlpha() {
        return alpha;
    }

    public void setAlpha(Wolf alpha) {
        this.alpha = alpha;
    }

    public Wolf getBeta() {
        return beta;
    }

    public void setBeta(Wolf beta) {
        this.beta = beta;
    }

    public Wolf getDelta() {
        return delta;
    }

    public void setDelta(Wolf delta) {
        this.delta = delta;
    }
}
