import numpy as np
import random
import function

lower = -5.12
upper = 5.12
iterations = 1000
pop_size = 100
dimensions = 2
ro = 2
eta = 1


def SWA():
    position = np.random.uniform(0, 1, (pop_size, dimensions)) * (upper - lower) + lower
    fitness = np.empty(pop_size)
    for i in range(pop_size):
        fitness[i] = function.function(position[i])

    best_fitness = fitness[np.argmin(fitness)]
    for x in range(iterations):
        for i in range(pop_size):
            y = better_and_nearest(i, position, fitness)
            if y:
                param = ro * np.exp(-eta * distance(position, i, y))
                newpos = position[i] + random.uniform(0, param) * (position[
                                                                       y] - position[i])
                position[i] = np.copy(np.clip(newpos, lower, upper))
           # print(position[i])
            fitness[i] = function.function(position[i])
        print(fitness[np.argmin(fitness)])
    return fitness[np.argmin(fitness)]


def distance(population, i, j):
    return np.linalg.norm(population[i] - population[j])


def better_and_nearest(u, population, fitness):
    v = 0
    temp = float("Inf")
    for i in range(len(population)):
        if i != u:
            if fitness[i] < fitness[u]:
                dist = np.linalg.norm(population[i] - population[u])
                if dist < temp:
                    v = i
                    temp = dist

    return v


print(SWA())
