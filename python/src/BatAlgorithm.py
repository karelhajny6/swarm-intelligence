import numpy as np
import random
import function as f
import math
import matplotlib.pyplot as plt

iterations = 1000
alfa = 0.9
gama = 0.9
r_start = 0.1
lower_bound = -5.12
upper_bound = 5.12


class BatAlgorithm:

    def __init__(self, pop_size, dimensions, frequency_min, frequency_max):
        self.velocity = np.zeros([pop_size, dimensions])
        self.frequency = np.zeros(pop_size)
        self.frequency_min = frequency_min
        self.frequency_max = frequency_max
        self.solution = np.empty([pop_size, dimensions])
        self.fitness = np.empty(pop_size)
        self.pop_size = pop_size
        self.dimensions = dimensions
        self.loudness = np.ones(pop_size)
        self.puls_rate = np.empty(pop_size)
#asdasd
        for i in range(pop_size):
            for j in range(dimensions):
                self.solution[i][j] = random.uniform(lower_bound, upper_bound)
            self.fitness[i] = f.function(self.solution[i])
            self.puls_rate[i] = r_start  # nastaveni pulzu, mozno zkusit nastavit fixne

        self.index, self.best = self.find_best()
        self.best_fitness = self.fitness[self.index]

    def find_best(self):
        index = np.argmin(self.fitness)
        return index, self.solution[index]

    def run_algorithm(self):
        for x in range(iterations):
            # print(self.solution)
            #print(self.frequency)
            #print(self.fitness[self.index])
            # print(self.best)
            # print(f.function(self.best))
            # print("-------")
            sol = np.empty([self.pop_size, self.dimensions])
            for i in range(self.pop_size):
                self.frequency[i] = self.frequency_min + (self.frequency_min - self.frequency_max) * random.random()
                self.velocity[i][:] = self.velocity[i][:] + (self.solution[i][:] - self.best[:]) * self.frequency[i]
                sol[i][:] = self.solution[i][:] + self.velocity[i][:]
                np.clip(sol[i], -5.12, 5.12)
                if random.random() > self.puls_rate[i]:
                    for j in range(self.dimensions):
                        sol[i][j] = self.best[j] + 0.01 * random.random() * np.average(self.loudness)

                new_fitness = f.function(sol[i])
                if (new_fitness < self.fitness[i]) and (self.loudness[i] > random.random()):
                    self.solution[i][:] = sol[i][:]
                    self.fitness[i] = new_fitness
                    self.loudness[i] = alfa * self.loudness[i]
                    self.puls_rate[i] = r_start * (1 - math.exp(-gama * (x + 1)))
                if new_fitness < self.fitness[self.index]:
                    self.best[:] = sol[i][:]
                    self.solution[i][:] = sol[i][:]
                    self.index = i

                plt.scatter(self.solution[:][0], self.solution[:][1])
                plt.pause(0.05)

            plt.show()

        print("Nejlepsi fitness je:" + str(f.function(self.best)))
        return self.best


b = BatAlgorithm(25, 2, 0, 1)

print(b.run_algorithm())
